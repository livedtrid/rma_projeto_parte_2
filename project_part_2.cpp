/*
 * Mixed Reality and Applications
 *
 * Pedro Santana
 * 2013-2017 ISCTE-IUL
 *
 *  Assumed frames of reference:
 *  - PCL: z-blue (into the scene), x-red (to the right), y-green (downwards)
 *  - OSG: z (upwards), x (to the left), y (out of the scene)
 *
 */

#pragma region includes
#include <boost/thread/thread.hpp>

#include <osg/Camera>
#include <osgDB/ReadFile>
#include <osgDB/WriteFile>
#include <osgViewer/Viewer>
#include <osg/Texture2D>
#include <osg/MatrixTransform>
#include <osg/GraphicsContext>
#include <osg/Depth>
#include <osg/Material>
#include <osg/Texture2D>
#include <osg/PositionAttitudeTransform>
#include <osg/Geometry>
#include <osg/Geode>
#include <osg/BlendFunc>
#include <osg/AlphaFunc>
#include <osgGA/GUIEventHandler>
#include <osg/ShapeDrawable>
#include <osgText/Font>
#include <osgText/Text>

#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/ModelCoefficients.h>
#include <pcl/common/transforms.h>
#include <pcl/common/eigen.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/visualization/cloud_viewer.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/search/kdtree.h>
#include <pcl/filters/passthrough.h>
#include <pcl/common/transforms.h>
#include <pcl/filters/statistical_outlier_removal.h>
#include <pcl/segmentation/extract_clusters.h>

#include <string>
#include <algorithm>
#include <sstream>

#include <iostream>

#include <boost/filesystem.hpp>
#include <dirent.h>

#pragma endregion INCLUDES

#pragma region shaders
static const char *textureVertexSource = {
    "varying vec3 normal;\n"
    "void main()\n"
    "{\n"
    "    gl_Position = gl_ProjectionMatrix * gl_ModelViewMatrix * gl_Vertex;\n"
    "}\n"};

static const char *textureFramentSource = {
    "uniform sampler2D texture;\n"
    "uniform sampler2D textureDepth;\n"
    "void main()\n"
    "{\n"
    "   vec2 uv = vec2(gl_FragCoord.x/640.0, gl_FragCoord.y/480.0);\n"
    "   gl_FragColor = texture2D(texture, uv);\n"
    "   gl_FragDepth = (texture2D(textureDepth, uv)[2]);\n"
    "}\n"};
#pragma endregion shaders

///
/// \brief estimateCameraPose Método que irá estimar a posição da camera e devolver a sua altura e rotação
/// \param cloud_in Nuvem de pontos de entrada (Background)
/// \param pitch
/// \param roll
/// \param height
///
void estimateCameraPose(pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_in, double *pitch, double *roll, double *height)
{

    // Seleciona um conjunto de pontos na zona imediatamente à frente da câmera, com a premissa de que não estarão objetos sobre o plano.
    // A zona é definida como um quadrado 100x100 pixeis.
    // No código seguinte As coordenadas dos pixeis são indexadas ao vetor de pontos
    pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_centre(new pcl::PointCloud<pcl::PointXYZRGBA>);

    cloud_centre->height = 1;
    int step = 2;
    long index;

    for (int row = 0; row < cloud_in->height / 4; row += step)
    {

        for (int col = cloud_in->width / 2 - 50; col < cloud_in->width / 2 + 50; col += step)
        {

            index = (cloud_in->height - row - 1) * cloud_in->width + col;

            cloud_centre->points.push_back(cloud_in->points[index]);

            cloud_centre->width++;
        }
    }

    // Guarda a nuvem de pontos para um ficheiro
    pcl::PCDWriter writer;

    writer.write<pcl::PointXYZRGBA>("Out/cloud_centre.pcd", *cloud_centre, false);

    // Determina os coeficientes do plano que melhor se aproxima do conjunto de planos.
    pcl::ModelCoefficients::Ptr coefficients(new pcl::ModelCoefficients());

    pcl::PointIndices::Ptr inliers(new pcl::PointIndices());

    pcl::SACSegmentation<pcl::PointXYZRGBA> seg;

    seg.setModelType(pcl::SACMODEL_PLANE);

    seg.setMethodType(pcl::SAC_RANSAC); //Método RANSAC

    seg.setMaxIterations(10000);

    seg.setDistanceThreshold(0.05);

    seg.setInputCloud(cloud_centre);

    seg.segment(*inliers, *coefficients);

    // Extrai os coeficientes e apresenta no ecrã
    double c_a = coefficients->values[0];

    double c_b = coefficients->values[1];

    double c_c = coefficients->values[2];

    double c_d = coefficients->values[3];

    std::cout << "Coefficients a: " << c_a << " b: " << c_b << " c: " << c_c << " d: " << c_d << "." << std::endl;

    // Garante que os coeficientes definem um vetor de comprimento unitário
    double norm = sqrt(c_a * c_a + c_b * c_b + c_c * c_c);

    c_a = c_a / norm;

    c_b = c_b / norm;

    c_c = c_c / norm;

    std::cout << "Coefficients a: " << c_a << " b: " << c_b << " c: " << c_c << " d: " << c_d << " norm: " << norm << std::endl;

    //Inversão de sinal
    if (c_c < 0)
    {

        c_a *= -1;
        c_b *= -1;
        c_c *= -1;
    }

    //Determina a inclinação do plano da mesa relativamente ao sensor.
    *pitch = asin(c_c);

    *roll = -acos(c_b / cos(*pitch));

    *height = fabs(c_d);

    //8.B. Inversão de sinal
    if (c_a < 0)

        (*roll) *= -1;

    //Apresenta no ecrã as variáveis obtidas
    std::cout << "Camera pitch: " << *pitch * 180 / M_PI << " [deg]; Camera roll: " << *roll * 180 / M_PI << " [deg]." << std::endl;

    std::cout << "Camera height: " << *height << std::endl;
}

///
/// \brief voxelizePointCloud Método que reduz o número de pontos utilizando uma voxel grid
/// \param cloud_in Nuvem de pontos de entrada
/// \param cloud_voxelised Nuvem de pontos voxelizada
///
void voxelizePointCloud(pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_in, pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_voxelised)
{
    // Reduz o número de pontos atraves da utilização de uma Voxel Grid

    pcl::VoxelGrid<pcl::PointXYZRGBA> voxel_grid;

    voxel_grid.setInputCloud(cloud_in);

    voxel_grid.setLeafSize(0.01, 0.01, 0.01);

    voxel_grid.filter(*cloud_voxelised);

    // Escreve o ficheiro com a nuvem de pontos vexelizada
    pcl::PCDWriter writer;

    writer.write<pcl::PointXYZRGBA>("Out/out_voxelised.pcd", *cloud_voxelised, false);
}

///
/// \brief removeOutliers Remove os outliers de uma point cloud utilizando o método que considera um outlier, um ponto cuja a média das distâncias aos seus vizinhos mais proximos seja muito diferente das distâncias médias observadas nos vários pontos das nuvens... respira
/// \param cloud_in Será a nuvem de pontos de entrada
/// \param cloud_clean Nuvem de ponto de saida sem os outliers
///
void removeOutliers(pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_in, pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_clean)
{
    // filtro para remoção de outliers
    pcl::StatisticalOutlierRemoval<pcl::PointXYZRGBA> sor;

    sor.setInputCloud(cloud_in);

    sor.setMeanK(50);

    sor.setStddevMulThresh(5.0); //3.0

    sor.filter(*cloud_clean);

    // Escreve o ficheiro com a nuvem de pontos limpa
    pcl::PCDWriter writer;

    writer.write<pcl::PointXYZRGBA>("Out/out_clean.pcd", *cloud_clean, false);
    //pcl_viewer Out/out_clean.pcd -ax 1
}

///
/// \brief rotatePointCloud Roda a nuvem de pontos
/// \param cloud_in Nuvem de ponto de entrada
/// \param cloud_rotated Será a nuvem rodada
/// \param camera_pitch inclinação da camera
/// \param camera_roll rotação da camera
/// \param camera_height altura da camera
///
void rotatePointCloud(pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_in,
                      pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_rotated,
                      double camera_pitch, double camera_roll, double camera_height)
{
    // Alinha a nuvem de pontos voxelizada ao sensor. O código a seguir cria 3 matrizes de transformação que depois são concatenadas em uma única matriz através de multiplicação
    Eigen::Affine3f t1 = pcl::getTransformation(0.0, -camera_height, 0.0, 0.0, 0.0, 0);

    Eigen::Affine3f t2 = pcl::getTransformation(0.0, 0.0, 0.0, -camera_pitch, 0.0, 0.0);

    Eigen::Affine3f t3 = pcl::getTransformation(0.0, 0.0, 0.0, 0.0, 0.0, -camera_roll);

    pcl::transformPointCloud(*cloud_in, *cloud_rotated, t1 * t2 * t3);

    // Escreve o ficheiro com a nuvem de pontos rodada
    pcl::PCDWriter writer;

    writer.write<pcl::PointXYZRGBA>("Out/out_rotated.pcd", *cloud_rotated, false);
    //pcl_viewer Out/out_rotated.pcd -ax 1
}

///
/// \brief clipPointCloud filtra todos os pontos cuja a coordenada y, x ou z esteja fora dos limites estabelecidos
/// \param cloud_in
/// \param cloud_clipped
/// \param fieldName A coordenada a aplicar o filtro, x, y ou z
/// \param limit_min O limite mínimo, default é -0.1
/// \param limit_max O limite máximo, default é 0.1
///
void clipPointCloud(pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_in, pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_clipped, std::string fieldName, float limit_min = -0.1, float limit_max = 0.1, bool isNegative = false)
{
    if (fieldName != "x" && fieldName != "y" && fieldName != "z")
    {
        PCL_ERROR("O fieldName deve ser um dos eixos x, y ou z");
    }
    pcl::PassThrough<pcl::PointXYZRGBA> pass;
    pass.setInputCloud(cloud_in);
    pass.setFilterFieldName(fieldName);
    pass.setFilterLimitsNegative(isNegative);
    pass.setFilterLimits(limit_min, limit_max); //limites
    pass.filter(*cloud_clipped);

    // Escreve o ficheiro com a nuvem de pontos cortada
    pcl::PCDWriter writer;

    writer.write<pcl::PointXYZRGBA>("Out/out_clipped.pcd", *cloud_clipped, false);
    //pcl_viewer Out/out_clipped.pcd -ax 1 //Isso é para facilitar o debug, poder ver as clouds criadas em cada passo
}

///
/// \brief extractPlaneFromPointCloud Aplica RANSAC para encontrar e extrair o plano dominante da point cloud
/// \param cloud_in A point cloud de entrada
/// \param cloud_on_plane A point cloud extraida, se tudo correr bem deverá ter a forma de um plano
/// \param coefficients Os coeficientes que serão calculados aqui nos serão uteis mais a frente por isso vamos manter uma referência desses valores
/// \param inliers Os inliers encontrados durante a segmentação do plano também serão uteis posteriormente
/// \param seg Não seria necessário ter o SACSegmentation como parametro (Retirar posteriormente e criar localmente, deverá poupar na memória acredito eu)
///
void extractPlaneFromPointCloud(pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_in, pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_on_plane, pcl::ModelCoefficients::Ptr coefficients, pcl::PointIndices::Ptr inliers, pcl::SACSegmentation<pcl::PointXYZRGBA> seg)
{
    //Determina o plano dominante
    seg.setOptimizeCoefficients(true);
    seg.setModelType(pcl::SACMODEL_PLANE);
    seg.setMethodType(pcl::SAC_RANSAC); //Detecção baseada no método RANSAC
    seg.setDistanceThreshold(0.01);
    seg.setInputCloud(cloud_in);
    seg.segment(*inliers, *coefficients);

    pcl::ExtractIndices<pcl::PointXYZRGBA> extract;
    extract.setInputCloud(cloud_in);
    extract.setIndices(inliers);
    extract.setNegative(false);
    extract.filter(*cloud_on_plane);

    // Escreve o ficheiro com o plano extraido da nuvem de pontos
    pcl::PCDWriter writer;

    writer.write<pcl::PointXYZRGBA>("Out/out_cloud_on_plane.pcd", *cloud_on_plane, false);
}

///
/// \brief discartPlaneFromPointCloud Irá remover os pontos correspondentes aos indices de uma point cloud
/// \param cloud_in a point cloud de entrada
/// \param cloud_off_plane A point cloud sem os pontos
/// \param inliers Index dos pontos a serem removidos
///
void discartPlaneFromPointCloud(pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_in, pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_off_plane, pcl::PointIndices::Ptr inliers)
{
    pcl::ExtractIndices<pcl::PointXYZRGBA> extract;
    extract.setInputCloud(cloud_in);
    extract.setIndices(inliers);
    extract.setNegative(true);
    extract.filter(*cloud_off_plane);

    // Escreve o ficheiro com a nuvem de pontos sem o plano
    pcl::PCDWriter writer;

    writer.write<pcl::PointXYZRGBA>("Out/out_cloud_off_plane.pcd", *cloud_off_plane, false);
    //pcl_viewer Out/out_cloud_off_plane.pcd -ax 1
}

///
/// \brief groupPointCloud Guarda em disco todos os cluster de point cloud encontrados
/// \param cloud_in nuvem de pontos de entrada
///
void savePointCloudClusters(pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_in)
{
    //Passa a point cloud por uma KdTree
    pcl::search::KdTree<pcl::PointXYZRGBA>::Ptr tree(new pcl::search::KdTree<pcl::PointXYZRGBA>);
    tree->setInputCloud(cloud_in);

    std::vector<pcl::PointIndices> cluster_indices;

    pcl::EuclideanClusterExtraction<pcl::PointXYZRGBA> ec;

    ec.setClusterTolerance(0.02);
    ec.setMinClusterSize(10);
    ec.setMaxClusterSize(25000);
    ec.setSearchMethod(tree);
    ec.setInputCloud(cloud_in);
    ec.extract(cluster_indices);

    std::cout << "cluster_indices =" << cluster_indices.size() << std::endl;

    int max_size = 0;
    pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_cluster_selected(new pcl::PointCloud<pcl::PointXYZRGBA>);

    for (int i = 0; i < cluster_indices.size(); i++)
    {
        pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_cluster(new pcl::PointCloud<pcl::PointXYZRGBA>);
        for (int j = 0; j < cluster_indices[i].indices.size(); j++)
        {
            cloud_cluster->points.push_back(cloud_in->points[cluster_indices[i].indices[j]]);
        }
        cloud_cluster->width = cloud_cluster->points.size();
        cloud_cluster->height = 1;
        cloud_cluster->is_dense = true;

        if (cloud_cluster->size() > max_size)
        {
            max_size = cloud_cluster->size();
            *cloud_cluster_selected = *cloud_cluster;
        }

        // Escreve o ficheiro com a nuvem de pontos sem o plano
        pcl::PCDWriter writer;

        std::string fileNumber = boost::to_string(i);

        writer.write<pcl::PointXYZRGBA>("Out/cloud_cluster_" + fileNumber + ".pcd", *cloud_cluster, false);
    }
}

///
/// \brief getLargerPointCloudCluster devolve o maior cluster da point cloud
/// \param cloud_in nuvem de pontos de entrada
/// \return Maior cluster encontrado
///
pcl::PointCloud<pcl::PointXYZRGBA>::Ptr getLargerPointCloudCluster(pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_in)
{
    //Passa a point cloud por uma KdTree
    pcl::search::KdTree<pcl::PointXYZRGBA>::Ptr tree(new pcl::search::KdTree<pcl::PointXYZRGBA>);
    tree->setInputCloud(cloud_in);

    std::vector<pcl::PointIndices> cluster_indices;

    pcl::EuclideanClusterExtraction<pcl::PointXYZRGBA> ec;

    ec.setClusterTolerance(0.02);
    ec.setMinClusterSize(10);
    ec.setMaxClusterSize(25000);
    ec.setSearchMethod(tree);
    ec.setInputCloud(cloud_in);
    ec.extract(cluster_indices);

    std::cout << "cluster_indices =" << cluster_indices.size() << std::endl;

    int max_size = 0;
    pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_cluster_selected(new pcl::PointCloud<pcl::PointXYZRGBA>);

    for (int i = 0; i < cluster_indices.size(); i++)
    {
        pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_cluster(new pcl::PointCloud<pcl::PointXYZRGBA>);
        for (int j = 0; j < cluster_indices[i].indices.size(); j++)
        {
            cloud_cluster->points.push_back(cloud_in->points[cluster_indices[i].indices[j]]);
        }
        cloud_cluster->width = cloud_cluster->points.size();
        cloud_cluster->height = 1;
        cloud_cluster->is_dense = true;

        if (cloud_cluster->size() > max_size)
        {
            max_size = cloud_cluster->size();
            *cloud_cluster_selected = *cloud_cluster;
        }
    }

    return cloud_cluster_selected;
}

///
/// \brief getHands mesma coisa que o getLargerPointCloudCluster devolve o maior cluster da point cloud vou deixar num método separado porque precisamos da quantidade de mãos
/// \param cloud_in nuvem de pontos de entrada
/// \return Maior cluster encontrado
///
pcl::PointCloud<pcl::PointXYZRGBA>::Ptr getHands(pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_in, bool *isConfirmationHandVisible)
{
    //Passa a point cloud por uma KdTree
    pcl::search::KdTree<pcl::PointXYZRGBA>::Ptr tree(new pcl::search::KdTree<pcl::PointXYZRGBA>);
    tree->setInputCloud(cloud_in);

    std::vector<pcl::PointIndices> cluster_indices;

    pcl::EuclideanClusterExtraction<pcl::PointXYZRGBA> ec;

    ec.setClusterTolerance(0.02);
    ec.setMinClusterSize(30);
    ec.setMaxClusterSize(25000);
    ec.setSearchMethod(tree);
    ec.setInputCloud(cloud_in);
    ec.extract(cluster_indices);

    std::cout << "Number of Hands =" << cluster_indices.size() << std::endl;

    if (cluster_indices.size() > 1)
    {
        *isConfirmationHandVisible = true;
    }
    else
    {
        *isConfirmationHandVisible = false;
    }

    int max_size = 0;
    pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_cluster_selected(new pcl::PointCloud<pcl::PointXYZRGBA>);

    Eigen::Affine3f transform;

    float x_min;

    //pcl::transformPointCloud()
    for (int i = 0; i < cluster_indices.size(); i++)
    {

        pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_cluster(new pcl::PointCloud<pcl::PointXYZRGBA>);
        for (int j = 0; j < cluster_indices[i].indices.size(); j++)
        {
            cloud_cluster->points.push_back(cloud_in->points[cluster_indices[i].indices[j]]);
        }
        cloud_cluster->width = cloud_cluster->points.size();
        cloud_cluster->height = 1;
        cloud_cluster->is_dense = true;

        //std::cout << "cloud_cluster size =" << cloud_cluster->size() << std::endl;
        if (cloud_cluster->size() > max_size)
        {
            max_size = cloud_cluster->size();
            *cloud_cluster_selected = *cloud_cluster;
        }
    }

    return cloud_cluster_selected;
}

///
/// \brief getObjectType Irá devolver o tipo do objeto reconhecido na point cloud
/// \param cloud_in nuvem de pontos de entrada
/// \return Tipo do objeto (0=Marcador || 1=Apagador)
///
int getObjectType(pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_in)
{
    int value = -1;

    //Encontrar o ponto mais próximo da camera
    int closest_index;
    double z_min = 10000;
    for (int i = 0; i < cloud_in->size(); i++)
    {
        if (cloud_in->points[i].z < z_min && cloud_in->points[i].z > 0.1)
        {
            closest_index = i;
            z_min = cloud_in->points[i].z;
        }
    }

    pcl::search::KdTree<pcl::PointXYZRGBA> kdtree; //Cria uma kdtree para fazer uma pesquisa dos pontos vizinhos ao ponto mais próximo da camera

    std::vector<int> pointIdxSearch;

    std::vector<float> pointSquaredDistance; //???

    kdtree.setInputCloud(cloud_in);

    kdtree.radiusSearch(cloud_in->points[closest_index], 0.1, pointIdxSearch, pointSquaredDistance);

    if (pointIdxSearch.size() > 160) //Pelos testes as nuvens com o apagador dão sempre um valor superior a 150
    {
        value = 1; //Se for um valor elevado é um apagador
        std::cout << " Objeto detectado: Apagador" << std::endl;
    }
    else
    {
        value = 0; // Se for baixo será o giz provavelmente
        std::cout << "Objeto detectado: Marcador" << std::endl;
    }

    return value;
}

///
/// \brief getPointCloudSize Irá devolver o tamanho da mesa, os valores deverão ser utilizados para remover os pontos da point cloud com os objetos. Visto que não valem a pena serem considerados.
/// \param cloud_in A Deverá ser a point cloud somente com o plano da mesa, já limpa, rodada e segmentada.
/// \param left_limit O apontador para o valor em x do ponto mais a esquerda
/// \param right_limit O apontador para o valor em x do ponto mais a direita
/// \param backward_limit O apontador para o valor em z do ponto mais próximo da câmera
/// \param foward_limit O apontador para o valor em z do ponto mais afastado da mesa
///
void getPointCloudSize(pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_in, float *left_limit, float *right_limit, float *backward_limit, float *foward_limit)
{

    int closest_index;
    double z_min = 10000;

    int farthest_index;
    double z_max = 0;

    int left_most_index;
    double x_min = 5000;

    int right_most_index;
    double x_max = -5000;

    for (int i = 0; i < cloud_in->size(); i++)
    {
        //Encontra o ponto mais próximo da camera
        if (cloud_in->points[i].z < z_min && cloud_in->points[i].z > 0.1)
        {
            closest_index = i;
            z_min = cloud_in->points[i].z;
        }

        //Encontra o ponto mais afastado da camera
        if (cloud_in->points[i].z > z_max && cloud_in->points[i].z < 1000) //Cheio de números mágicos por aqui
        {
            farthest_index = i; //you said fart
            z_max = cloud_in->points[i].z;
        }
        //Encontra o ponto mais a esquerda
        if (cloud_in->points[i].x < x_min && cloud_in->points[i].x > -5000)
        {
            left_most_index = i;
            x_min = cloud_in->points[i].x;
        }
        //Encontra o ponto mais a direita
        if (cloud_in->points[i].x > x_max && cloud_in->points[i].x < 5000)
        {
            right_most_index = i;
            x_max = cloud_in->points[i].x;
        }
    }

    //std::cout << cloud_in->points[closest_index].x << " " << cloud_in->points[closest_index].y << " " << cloud_in->points[closest_index].z << std::endl;
    //std::cout << cloud_in->points[farthest_index].x << " " << cloud_in->points[farthest_index].y << " " << cloud_in->points[farthest_index].z << std::endl;
    //std::cout << cloud_in->points[left_most_index].x << " " << cloud_in->points[left_most_index].y << " " << cloud_in->points[left_most_index].z << std::endl;
    //std::cout << cloud_in->points[right_most_index].x << " " << cloud_in->points[right_most_index].y << " " << cloud_in->points[right_most_index].z << std::endl;

    *left_limit = cloud_in->points[left_most_index].x;
    *right_limit = cloud_in->points[right_most_index].x;
    *backward_limit = cloud_in->points[closest_index].z;
    *foward_limit = cloud_in->points[farthest_index].z;
}

///
/// \brief getCloudAbovePlane Esse método deverá receber a segunda point cloud passada por argumento e com os coeficientes e inliers do plano da mesa, irá devolver uma point cloud com os pontos que estão acima desse plano.
/// \param cloud_in A point cloud de entrada, será o segundo argumento passado pela linha de comandos se houver
/// \param cloud_above_plane A a point cloud de saida terá os pontos acima do plano da mesa
/// \param coefficients Os coeficientes a serem utilizados são os que foram calculados com base na primeira point cloud, aquela sem os objetos.
/// \param inliers Os inliers são também da primeira point cloud sem os objetos
/// \param seg Envia o objeto para segmentação criado no Main (Poderia ser local)
///
void getCloudAbovePlane(pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_in, pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_above_plane, pcl::ModelCoefficients::Ptr coefficients, pcl::PointIndices::Ptr inliers, pcl::SACSegmentation<pcl::PointXYZRGBA> seg)
{
    pcl::ExtractIndices<pcl::PointXYZRGBA> extract;

    //pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_tabletop(new pcl::PointCloud<pcl::PointXYZRGBA>);
    seg.setInputCloud(cloud_in);
    //seg.segment(*inliers, *coefficients); // Olha que legal, estamos usando os inliers e coeficientes da point_cloud que tem somente a mesa. Temos que perguntar ao professor se é isso mesmo. Há uns pontos a mais também será preciso mais algum filtro?
    extract.setInputCloud(cloud_in);
    extract.setIndices(inliers);
    extract.setNegative(false);
    //extract.filter(*cloud_tabletop); //Vou deixar comentado caso seja preciso ver essa point cloud posteriormente

    double a = coefficients->values[0];
    double b = coefficients->values[1];
    double c = coefficients->values[2];
    double d = coefficients->values[3];

    for (int i = 0; i < cloud_in->size(); i++)
    {
        pcl::PointXYZRGBA p = cloud_in->points[i];
        if (a * p.x + b * p.y + c * p.z + d > 0.01)
        {
            cloud_above_plane->push_back(p);
        }
    }
}

typedef struct __s_RGB
{
    int r, g, b;
} RGB;

///
/// \brief getColorAverage Método que retorna a cor média de uma point cloud
/// \param cloud_in Nuvem de pontos de entrada
/// \return struct com a cor média da point cloud
///
RGB getColorAverage(pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_in)
{
    int x, y, z;
    RGB m;
    m.r = m.g = m.b = 0; //Se não inicializar a 0 aparece uns números random... coisas do C++
    //std::cout << "cloud_in->size: " << cloud_in->size() << std::endl;

    long index2;
    for (int row = 0; row < cloud_in->height; row++)
    {
        for (int col = 0; col < cloud_in->width; col++)
        {
            index2 = (cloud_in->height - row - 1) * cloud_in->width + col;

            uint8_t r, g, b;

            r = (int)cloud_in->points[index2].r;
            g = (int)cloud_in->points[index2].g;
            b = (int)cloud_in->points[index2].b;

            // Guarda a informação de cor de um dado ponto 3D
            m.r += r;

            m.g += g;

            m.b += b;
        }
    }
    m.r = m.r / cloud_in->size();
    m.g = m.g / cloud_in->size();
    m.b = m.b / cloud_in->size();
    //std::cout << " R: " << m.r << " G: " << m.g << " B: " << m.b << std::endl;

    return m;
}

///
/// \brief createImageFromPointCloud Método para criar uma imagem RGB com informação de profundidade
/// \param cloud_in Nuvem de pontos de entrada
/// \param data informação de cor de um ponto
/// \param dataDepth Informação de profundidade
///
void createImageFromPointCloud(pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_in, unsigned char *data, unsigned char *dataDepth)
{
    long index1, index2;
    for (int row = 0; row < cloud_in->height; row++)
    {
        for (int col = 0; col < cloud_in->width; col++)
        {
            index1 = 3 * (row * cloud_in->width + col);
            index2 = (cloud_in->height - row - 1) * cloud_in->width + col;

            // Guarda a informação de cor de um dado ponto 3D
            data[index1] = cloud_in->points[index2].r;

            data[index1 + 1] = cloud_in->points[index2].g;

            data[index1 + 2] = cloud_in->points[index2].b;

            // Guarda a informação de profundidade. isnan filtra somente os pixeis que tem informação 3D
            if (isnan(cloud_in->points[index2].x))
            {

                dataDepth[index1] = dataDepth[index1 + 1] = dataDepth[index1 + 2] = 0;
            }
            else
            {

                dataDepth[index1] = ((cloud_in->points[index2].x + 2) / 6.0) * 255.0;

                dataDepth[index1 + 1] = ((cloud_in->points[index2].y + 2) / 6.0) * 255.0;

                dataDepth[index1 + 2] = (cloud_in->points[index2].z / 10.0) * 255.0;
            }
        }
    }
}

///
/// \brief createVirtualCameras Método que cria as cameras virtuais
/// \param camera1 câmera ortográfica responsável por olhar para o quad com a imagem produzida pelo sensor
/// \param camera2 câmera com perspectiva que irá olhar para os objetos virtuais
/// \param orthoTexture textura para câmera ortográfica
/// \param camera_pitch
/// \param camera_roll
/// \param camera_height
///
void createVirtualCameras(osg::ref_ptr<osg::Camera> camera1, osg::ref_ptr<osg::Camera> camera2, osg::ref_ptr<osg::Geode> orthoTexture,
                          double camera_pitch, double camera_roll, double camera_height)
{
    // Cria uma câmera ortográfica responsável por olhar para o quad com a imagem produzida pelo sensor.
    camera1->setCullingActive(false);

    camera1->setClearMask(0);

    camera1->setAllowEventFocus(false);

    camera1->setReferenceFrame(osg::Camera::ABSOLUTE_RF);

    camera1->setRenderOrder(osg::Camera::POST_RENDER, 0); //Configura a câmera de forma a que seja a primeira a produzir uma imagem para o seu output seja apresentado em background

    camera1->setProjectionMatrix(osg::Matrix::ortho(0.0, 1.0, 0.0, 1.0, 0.5, 1000));

    camera1->addChild(orthoTexture.get());

    osg::StateSet *ss = camera1->getOrCreateStateSet();

    ss->setMode(GL_LIGHTING, osg::StateAttribute::OFF);

    // Cria uma câmera com perspectiva que irá olhar para os objetos virtuais
    camera2->setCullingActive(false);

    camera2->setClearMask(0);

    camera2->setAllowEventFocus(false);

    camera2->setReferenceFrame(osg::Camera::ABSOLUTE_RF);

    camera2->setRenderOrder(osg::Camera::POST_RENDER, 1); // Configura a câmera de forma a que o seu output seja sobreposto ao da câmera ortográfica

    camera2->setProjectionMatrixAsPerspective(50, 640. / 480., 0.5, 1000);

    // Cria uma matriz de rotação que terá em conta o ângulo do pitch da câmera
    osg::Matrixd cameraRotation2;

    cameraRotation2.makeRotate(osg::DegreesToRadians(camera_pitch * 180 / M_PI), osg::Vec3(1, 0, 0),

                               osg::DegreesToRadians(0.0), osg::Vec3(0, 1, 0),

                               osg::DegreesToRadians(0.0), osg::Vec3(0, 0, 1));

    // Cria uma matriz de rotação que terá em conta o ângulo do roll da câmera
    osg::Matrixd cameraRotation3;

    cameraRotation3.makeRotate(osg::DegreesToRadians(0.0), osg::Vec3(1, 0, 0),

                               osg::DegreesToRadians(camera_roll * 180 / M_PI), osg::Vec3(0, 1, 0),

                               osg::DegreesToRadians(0.0), osg::Vec3(0, 0, 1));

    // Aplica uma translação que tem em conta a altura da câmera em relação ao plano encontrado
    osg::Matrixd cameraTrans;

    cameraTrans.makeTranslate(0, 0, camera_height * 100.0); //100 compensa a diferença de escalas entre OSG e PCL

    // Garante que as diferenças entre os referenciais do OSG e PCL são tidas em conta.
    osg::Matrix matrix_view;

    matrix_view.makeLookAt(osg::Vec3(0, 0, 0), osg::Vec3(0, -1, 0), osg::Vec3(0, 0, 1));

    osg::Matrixd cameraRotation;

    cameraRotation.makeRotate(osg::DegreesToRadians(180.0), osg::Vec3(0, 0, 1), osg::DegreesToRadians(-90.0), osg::Vec3(1, 0, 0), osg::DegreesToRadians(0.0), osg::Vec3(0, 1, 0));

    // Concatenação das matrizes a enviar para o OSG, a inversa é um requisito interno do OSG

    camera2->setViewMatrix(osg::Matrix::inverse(cameraRotation * cameraRotation3 * cameraRotation2 * cameraTrans));

    // Define o tamanho do viewport das câmeras
    camera1->setViewport(0, 0, 640, 480);

    camera2->setViewport(0, 0, 640, 480);
}

///
/// \brief createOrthoTexture Cria duas imagens que serão preenchidas com os vetores data e dataDepth
/// \param orthoTextureGeode geometria do OSG
/// \param data informação de cor
/// \param dataDepth informação de profundidade
/// \param width largura da textura
/// \param height altura da textura
///
void createOrthoTexture(osg::ref_ptr<osg::Geode> orthoTextureGeode,
                        unsigned char *data, unsigned char *dataDepth, int width, int height)
{
    //Cria duas imagens que serão preenchidas com os vetores data e dataDepth
    osg::Image *image = new osg::Image;

    osg::Image *imageDepth = new osg::Image;

    image->setOrigin(osg::Image::TOP_LEFT);

    imageDepth->setOrigin(osg::Image::TOP_LEFT);

    image->setImage(width, height, 1, GL_RGB, GL_RGB, GL_UNSIGNED_BYTE, data, osg::Image::NO_DELETE);

    imageDepth->setImage(width, height, 1, GL_RGB, GL_RGB, GL_UNSIGNED_BYTE, dataDepth, osg::Image::NO_DELETE);

    // Cria duas texturas com as imagens criadas anteriormente
    osg::ref_ptr<osg::Texture2D> texture = new osg::Texture2D;

    osg::ref_ptr<osg::Texture2D> texture2 = new osg::Texture2D;

    texture->setImage(image);

    texture2->setImage(imageDepth);

    // Cria um quad com largura e altura de 1 unidade, onde são associadas as duas texturas criadas anteriormente
    osg::ref_ptr<osg::Drawable> quad = osg::createTexturedQuadGeometry(osg::Vec3(), osg::Vec3(1.0f, 0.0f, 0.0f), osg::Vec3(0.0f, 1.0f, 0.0f));

    quad->getOrCreateStateSet()->setTextureAttributeAndModes(0, texture.get());

    quad->getOrCreateStateSet()->setTextureAttributeAndModes(1, texture2.get());

    // Cria os shaders
    osg::ref_ptr<osg::Shader> vertShader = new osg::Shader(osg::Shader::VERTEX, textureVertexSource); //Shader para processar os vértices do quad

    osg::ref_ptr<osg::Shader> fragShader = new osg::Shader(osg::Shader::FRAGMENT, textureFramentSource); //Shader para processar os fragmentos do quad

    osg::ref_ptr<osg::Program> program = new osg::Program;

    program->addShader(fragShader.get());

    program->addShader(vertShader.get());

    // Envia as texturas de cor e profundidade para os shaders através de variáveis globais uniform
    quad->setDataVariance(osg::Object::DYNAMIC);

    quad->getOrCreateStateSet()->setAttributeAndModes(program.get());

    quad->getOrCreateStateSet()->addUniform(new osg::Uniform("texture", 0));

    quad->getOrCreateStateSet()->addUniform(new osg::Uniform("textureDepth", 1));

    orthoTextureGeode->addDrawable(quad.get());
}

//Adaptado de: http://pointclouds.org/documentation/tutorials/pcl_visualizer.php
///
/// \brief rgbVis Método que retorna um viewer PCL
/// \param cloud A point cloud a ser visualizada
/// \return PCL_Viewer
///
boost::shared_ptr<pcl::visualization::PCLVisualizer> rgbVis(pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud)
{
    // --------------------------------------------
    // -----Open 3D viewer and add point cloud-----
    // --------------------------------------------
    boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer(new pcl::visualization::PCLVisualizer("3D Viewer"));
    viewer->setBackgroundColor(0, 0, 0);
    pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZRGBA> rgb(cloud);
    viewer->addPointCloud<pcl::PointXYZRGBA>(cloud, rgb, "sample cloud");
    viewer->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 3, "sample cloud");
    viewer->addCoordinateSystem(1.0);
    viewer->initCameraParameters();
    viewer->setSize(640, 480); //Para ficar bonitinho do tamanho da janela do osg
    viewer->setPosition(706, -52);
    return (viewer);
}

///
/// \brief getLowestPoint Método que retorna o ponto mais baixo da point cloud
/// \param cloud A point cloud de entrada
/// \return lowestPoint
///
pcl::PointXYZRGBA getLowestPoint(pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_in)
{

    pcl::PointXYZRGBA lowestPoint; //null ?

    //Encontrar o ponto mais próximo da mesa
    int closest_index;
    double y_min = -10000; //Isso tá invertido
    for (int i = 0; i < cloud_in->size(); i++)
    {
        //std::cout << "cloud_in->points[i].y: " << cloud_in->points[i].y << std::endl;

        if (cloud_in->points[i].y > y_min && cloud_in->points[i].y < -0.1)
        {
            closest_index = i;
            y_min = cloud_in->points[i].y;

            lowestPoint = cloud_in->points[i];
        }
    }

    return lowestPoint; //vai com as cores junto
}

///
/// \brief getDirection Método que retorna a direção do marcador
/// \param cloud A point cloud de entrada
/// \return direction
///
osg::Vec3 getDirection(pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_in)
{

    osg::Vec3 dir;
    osg::Vec3 dirNormalized;

    osg::Vec3 point_A; //Ponto mais proximo da camera
    osg::Vec3 point_B; //Ponto mais afastado da camera

    int closest_index;

    //Encontrar o ponto mais próximo da camera
    double z_min = 10000;
    for (int i = 0; i < cloud_in->size(); i++)
    {
        if (cloud_in->points[i].z < z_min && cloud_in->points[i].z > 0.1)
        {
            closest_index = i;
            z_min = cloud_in->points[i].z;

            point_A = osg::Vec3(cloud_in->points[i].x, 0, cloud_in->points[i].z);
        }
    }

    //Encontrar o ponto mais afastado da camera
    double z_max = 0;
    for (int i = 0; i < cloud_in->size(); i++)
    {
        if (cloud_in->points[i].z > z_max && cloud_in->points[i].z < 10000)
        {
            closest_index = i;
            z_max = cloud_in->points[i].z;

            point_B = osg::Vec3(cloud_in->points[i].x, 0, cloud_in->points[i].z);
        }
    }

    dir = point_A - point_B;

    double x = dir.x();
    double y = dir.y();
    double z = dir.z();

    double norm = sqrt(x * x + y * y + z * z);

    x = x / norm;

    y = y / norm;

    z = z / norm;

    dirNormalized = osg::Vec3(x, y, z);

    return dirNormalized;
}

bool detectCollisions(pcl::PointXYZRGBA startPoint, osg::Vec3 direction, pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_in, pcl::PointCloud<pcl::PointXYZRGBA>::Ptr intercepted_object)
{
    pcl::search::KdTree<pcl::PointXYZRGBA>::Ptr kdtree(new pcl::search::KdTree<pcl::PointXYZRGBA>);
    kdtree->setInputCloud(cloud_in);

    bool collided = false;

    osg::Vec3 startVector = osg::Vec3(startPoint.x, startPoint.y, startPoint.z);

    std::vector<int> search_indexes;
    std::vector<float> search_radiuses;

    //search_radiuses = 2.0f;

    osg::Vec3 checkVector;
    pcl::PointXYZRGBA checkPoint;

    float scale = 0.0f;

    while (scale < 1.0f)
    {
        checkVector = startVector + (direction.operator*(scale));
        checkPoint.x = checkVector.x();
        checkPoint.y = checkVector.y();
        checkPoint.z = checkVector.z();

        kdtree->radiusSearch(checkPoint, 0.3, search_indexes, search_radiuses);

        if (search_indexes.size() == 0)
        {
            collided = false;
        }
        else
        {
            collided = true;

            intercepted_object->width = search_indexes.size();

            intercepted_object->height = 1;

            intercepted_object->is_dense = false;

            for (int i = 0; i < search_indexes.size(); i++)
            {
                intercepted_object->push_back(cloud_in->points[search_indexes[i]]);
            }

            break;
        }

        scale += 0.1f;
    }

    std::cout << "collided: " << collided << std::endl;
    return collided;
}

// this function creates a text object, that will be used for the HUD
osgText::Text *createText(const osg::Vec3 &pos, const std::string &content, float size)
{
    //osg::ref_ptr<osgText::Font> g_font = osgText::readFontFile("fonts/arial.ttf");
    osg::ref_ptr<osgText::Text> text = new osgText::Text;
    //text->setFont(g_font.get());
    text->setCharacterSize(size);
    text->setAxisAlignment(osgText::TextBase::XY_PLANE);
    text->setPosition(pos);
    text->setText(content);
    // tell the system that the texto will be changing
    text->setDataVariance(osg::Object::DYNAMIC);
    return text.release();
}

// this function creates an orthographic camera that will be used as HUD
osg::ref_ptr<osg::Camera> createHUDCamera(double left, double right, double bottom, double top)
{
    osg::ref_ptr<osg::Camera> camera = new osg::Camera;
    // stating that this camera has a global position and, thus, not changeable by the transform hierarchy
    camera->setReferenceFrame(osg::Transform::ABSOLUTE_RF);
    camera->setClearMask(GL_DEPTH_BUFFER_BIT);
    // the next property ensures that the data rendered by this camera is overlaid on the output of other cameras
    camera->setRenderOrder(osg::Camera::POST_RENDER, 1);
    camera->setAllowEventFocus(false);
    // stating that this camera is orthographic
    camera->setProjectionMatrix(osg::Matrix::ortho2D(left, right, bottom, top));
    return camera;
}

// function responsible for creating the Head Up Display (HUD). this hud will present two texts to the user. one of them fixed, whereas the other
// will change at each new frame (in the truck's callback)
osg::ref_ptr<osg::Camera> createHUD(osg::ref_ptr<osgText::Text> *textFrames)
{

    // the geode responsible for storing the text (which is a form of geometry)
    osg::ref_ptr<osg::Geode> textGeode = new osg::Geode;

    // the second text will be made dynamic, so we use a global variable "textFrames" to store it
    // *textFrames = createText(osg::Vec3(150.0f, 400.0f, 0.0f), "", 20.0f);
    // associating this new text to the geode
    textGeode->addDrawable(*textFrames);

    //>>>TRY: create another text and add to the screen ...

    // calling a function (check it out) to create an orthographic camera, which will be rendered on the top of the viewing camera.
    // this camera will have as its single child the geode we've just created; this way, this camera will only see the two texts.
    osg::ref_ptr<osg::Camera> hudCamera = createHUDCamera(0, 1024, 0, 768);
    // attaching the text geode to the camera
    hudCamera->addChild(textGeode.get());
    // stating that there should be no illumination effects associated to this camera (we just want to see the text with flat shading)
    hudCamera->getOrCreateStateSet()->setMode(GL_LIGHTING, osg::StateAttribute::OFF);

    // return the camera object to be later on added to the scene graph
    return hudCamera;
}

///
/// \brief main
/// \param argsc
/// \param argsv 1º parametro obrigatório (point_cloud_sem_objetos) 2º parametro opcional (point_cloud_com_objetos) 3º parametro opcional (point_cloud_com_braço)
/// \return
///
int main(int argsc, char **argsv)
{

    std::stringstream debugText;

    /*
    //Retorna todos os ficheiros contidos numa pasta
    //Adaptado de https://stackoverflow.com/questions/306533/how-do-i-get-a-list-of-files-in-a-directory-in-c
    if (DIR *dir = opendir("../clouds_2018_draw/seq_1/draw_objs_1/"))
    {
        while (dirent *f = readdir(dir))
        {
            if (!f->d_name || f->d_name[0] == '.')
                continue; // Skip everything that starts with a dot
            std::cout << "file name =" << f->d_name << std::endl;
        }
        closedir(dir);
    }
    */

    pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_in(new pcl::PointCloud<pcl::PointXYZRGBA>); //cloud_in aqui será a cloud que servirá para criar a imagem RGBD

    pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_without_objects(new pcl::PointCloud<pcl::PointXYZRGBA>); // cloud_without_objects será a nuvem do background que vamos utilizar para fazer a estimativa da pose da camera.
    pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_with_objects(new pcl::PointCloud<pcl::PointXYZRGBA>);    // cloud_with_objects se for carregada será a cloud com o segundo background

    pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_with_hand(new pcl::PointCloud<pcl::PointXYZRGBA>);

    pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_to_viewer(new pcl::PointCloud<pcl::PointXYZRGBA>); //Guarda a cloud para debug

    pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_rotated(new pcl::PointCloud<pcl::PointXYZRGBA>);
    pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_voxelized(new pcl::PointCloud<pcl::PointXYZRGBA>);
    pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_clean(new pcl::PointCloud<pcl::PointXYZRGBA>);
    pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_clipped(new pcl::PointCloud<pcl::PointXYZRGBA>);
    pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_on_plane(new pcl::PointCloud<pcl::PointXYZRGBA>);
    pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_off_plane(new pcl::PointCloud<pcl::PointXYZRGBA>);
    pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_cluster_selected(new pcl::PointCloud<pcl::PointXYZRGBA>);
    pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_above_plane(new pcl::PointCloud<pcl::PointXYZRGBA>);

    pcl::PointCloud<pcl::PointXYZRGBA>::Ptr colored_object_A(new pcl::PointCloud<pcl::PointXYZRGBA>);
    pcl::PointCloud<pcl::PointXYZRGBA>::Ptr colored_object_B(new pcl::PointCloud<pcl::PointXYZRGBA>);
    pcl::PointCloud<pcl::PointXYZRGBA>::Ptr colored_object_C(new pcl::PointCloud<pcl::PointXYZRGBA>);

    pcl::PointCloud<pcl::PointXYZRGBA>::Ptr selected_object(new pcl::PointCloud<pcl::PointXYZRGBA>);

    RGB selected_object_color;
    osg::ref_ptr<osg::Geode> brush = new osg::Geode;

    // Carrega a nuvem de pontos que é passada como argumento do programa
    if (pcl::io::loadPCDFile(argsv[1], *cloud_without_objects) == -1)
    {

        PCL_ERROR("Couldn't read the PCD file \n");

        return (-1);
    }

    //Se houver um segundo argumento vamos assumir que se trata da point cloud com os objetos
    if (argsc > 2)
    {

        if (pcl::io::loadPCDFile(argsv[2], *cloud_with_objects) == -1)
        {
            PCL_ERROR("Couldn't read the PCD file \n");

            return (-1);
        }
        else
        {
            PCL_INFO("cloud_with_objects loaded \n");
        }

        //Se houver um terceiro argumento vamos assumir que se trata da nuvem com o braço
        if (argsc > 3)
        {
            if (pcl::io::loadPCDFile(argsv[3], *cloud_with_hand) == -1)
            {
                PCL_ERROR("Couldn't read the PCD file \n");

                return (-1);
            }
            else
            {
                PCL_INFO("cloud_with_hand loaded \n");
            }

            cloud_in = cloud_with_hand;
        }
        else
        {
            cloud_in = cloud_with_objects;
        }
    }
    else
    {
        cloud_in = cloud_without_objects;
        //cloud_to_viewer = cloud_without_objects;
    }

#pragma region PASSO_1

    // estimate the camera pose w.r.t. to ground plane
    // Variáveis para guardar a posição e inclinação da câmera
    double camera_pitch, camera_roll, camera_height;

    estimateCameraPose(cloud_without_objects, &camera_pitch, &camera_roll, &camera_height);

#pragma endregion PASSO_1

#pragma region PASSO_2
    //Vamos voxelizar (que palavra feia) a point cloud
    voxelizePointCloud(cloud_without_objects, cloud_voxelized);

    //Roda uma point cloud de forma a alinhar com o sensor
    rotatePointCloud(cloud_voxelized, cloud_rotated, camera_pitch, camera_roll, camera_height);

    removeOutliers(cloud_rotated, cloud_clean);

    //Fazemos clip dessa point cloud no eixo y com os valores default (-0.05 e 0.05) até que parece bem
    clipPointCloud(cloud_clean, cloud_clipped, "y", -0.05, 0.05);

    pcl::ModelCoefficients::Ptr coefficients(new pcl::ModelCoefficients);
    pcl::PointIndices::Ptr inliers(new pcl::PointIndices);
    pcl::SACSegmentation<pcl::PointXYZRGBA> seg;

    //Envia a cloud voxelizada e retorna o plano dominante em cloud_on_plane
    extractPlaneFromPointCloud(cloud_clipped, cloud_on_plane, coefficients, inliers, seg);

    discartPlaneFromPointCloud(cloud_clipped, cloud_off_plane, inliers);

    cloud_cluster_selected = getLargerPointCloudCluster(cloud_on_plane);

    float left_limit, right_limit, backward_limit, foward_limit;

    getPointCloudSize(cloud_cluster_selected, &left_limit, &right_limit, &backward_limit, &foward_limit);

    float table_width = table_width = right_limit - left_limit;        //Largura da mesa
    float table_lenght = table_lenght = foward_limit - backward_limit; //Comprimento da mesa

    std::cout << "table_width =" << table_width << std::endl;
    std::cout << "table_lenght =" << table_lenght << std::endl;

    debugText << "table_width =" << table_width << std::endl;
    debugText << "table_width =" << table_lenght << std::endl;

    // cloud_to_viewer = cloud_cluster_selected;

#pragma endregion PASSO_2

#pragma region PASSO_3

    //Se houver pontos na point cloud com os objetos
    if (cloud_with_objects->size() > 0)
    {
        pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_with_objects_rotated(new pcl::PointCloud<pcl::PointXYZRGBA>);
        pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_with_objects_voxelized(new pcl::PointCloud<pcl::PointXYZRGBA>);
        pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_with_objects_clean(new pcl::PointCloud<pcl::PointXYZRGBA>);
        pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_with_objects_clipped(new pcl::PointCloud<pcl::PointXYZRGBA>);
        pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_with_objects_on_plane(new pcl::PointCloud<pcl::PointXYZRGBA>);
        pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_with_objects_off_plane(new pcl::PointCloud<pcl::PointXYZRGBA>);
        pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_with_objects_cluster_selected(new pcl::PointCloud<pcl::PointXYZRGBA>);
        pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_with_objects_above_plane(new pcl::PointCloud<pcl::PointXYZRGBA>);

        //Claro, já ia esquecendo, é preciso passar os filtros para deixar ela mais leve vamos guardar na cloud_voxelized mesmo já que não iremos mais precisar dessa cloud
        voxelizePointCloud(cloud_with_objects, cloud_with_objects_voxelized);

        //Agora sim vamos guardar no cloud_rotated mesmo, se der barraca criamos um lugar especial para ela nas profundezas da memória
        rotatePointCloud(cloud_with_objects_voxelized, cloud_with_objects_rotated, camera_pitch, camera_roll, camera_height);

        removeOutliers(cloud_with_objects_rotated, cloud_with_objects_clean); //Isso

        clipPointCloud(cloud_with_objects_clean, cloud_with_objects_clipped, "x", left_limit, right_limit);        //Com base no tamanho da mesa vamos cortar a point cloud para ficar só com os pontos que interessam
        clipPointCloud(cloud_with_objects_clipped, cloud_with_objects_clipped, "z", backward_limit, foward_limit); //É feio mas funciona

        getCloudAbovePlane(cloud_with_objects_clipped, cloud_with_objects_above_plane, coefficients, inliers, seg); //Não era bem o que queríamos TODO: filtrar melhor os pontos, acho que me esqueci de algum passo

        extractPlaneFromPointCloud(cloud_with_objects_above_plane, cloud_with_objects_on_plane, coefficients, inliers, seg);

        discartPlaneFromPointCloud(cloud_with_objects_clean, cloud_with_objects_off_plane, inliers);

        //Como não estava conseguindo passar os indices dos cluster para extrai-los da cloud_above_plane optamos por escrever em disco os 3 ficheiros carrega-los aqui
        savePointCloudClusters(cloud_with_objects_on_plane);

        //Vamos guardar uma referencia a essa point cloud na variavel cloud_with_objects
        cloud_with_objects = cloud_with_objects_on_plane;

        if (pcl::io::loadPCDFile<pcl::PointXYZRGBA>("Out/cloud_cluster_0.pcd", *colored_object_A) == -1)
        {
            PCL_ERROR("Couldn't read file. \n");
            return (-1);
        }

        if (pcl::io::loadPCDFile<pcl::PointXYZRGBA>("Out/cloud_cluster_1.pcd", *colored_object_B) == -1)
        {
            PCL_ERROR("Couldn't read file. \n");
            return (-1);
        }
        //std::cout << "Loaded " << colored_object_B->width * colored_object_B->height << " points" << std::endl;

        if (pcl::io::loadPCDFile<pcl::PointXYZRGBA>("Out/cloud_cluster_2.pcd", *colored_object_C) == -1)
        {
            PCL_ERROR("Couldn't read file. \n");
            return (-1);
        }
        //std::cout << "Loaded " << colored_object_C->width * colored_object_C->height << " points" << std::endl;

        RGB m_color_a = getColorAverage(colored_object_A);
        RGB m_color_b = getColorAverage(colored_object_B);
        RGB m_color_c = getColorAverage(colored_object_C);

        std::cout << " Cor média (RGB) colored_object_A: "
                  << "(" << m_color_a.r << ", " << m_color_a.g << ", " << m_color_a.b << ")" << std::endl;
        std::cout << " Cor média (RGB) colored_object_B: "
                  << "(" << m_color_b.r << ", " << m_color_b.g << ", " << m_color_b.b << ")" << std::endl;
        std::cout << " Cor média (RGB) colored_object_C: "
                  << "(" << m_color_c.r << ", " << m_color_c.g << ", " << m_color_c.b << ")" << std::endl;

        cloud_to_viewer = cloud_with_objects;
    }

    pcl::ModelCoefficients cube_coeff;

#pragma endregion PASSO_3

#pragma region PASSO_4

    //Se houver pontos na cloud_with_hand
    if (cloud_with_hand->size() > 0)
    {
        pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_with_hand_rotated(new pcl::PointCloud<pcl::PointXYZRGBA>);
        pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_with_hand_voxelized(new pcl::PointCloud<pcl::PointXYZRGBA>);
        pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_with_hand_clean(new pcl::PointCloud<pcl::PointXYZRGBA>);
        pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_with_hand_clipped(new pcl::PointCloud<pcl::PointXYZRGBA>);
        pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_with_hand_on_plane(new pcl::PointCloud<pcl::PointXYZRGBA>);
        pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_with_hand_off_plane(new pcl::PointCloud<pcl::PointXYZRGBA>);
        pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_with_hand_cluster_selected(new pcl::PointCloud<pcl::PointXYZRGBA>);
        pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_with_hand_above_plane(new pcl::PointCloud<pcl::PointXYZRGBA>);

        voxelizePointCloud(cloud_with_hand, cloud_with_hand_voxelized);

        rotatePointCloud(cloud_with_hand_voxelized, cloud_with_hand_rotated, camera_pitch, camera_roll, camera_height);

        removeOutliers(cloud_with_hand_rotated, cloud_with_hand_clean);

        clipPointCloud(cloud_with_hand_clean, cloud_with_hand_clipped, "x", left_limit, right_limit);        //Com base no tamanho da mesa vamos cortar a point cloud para ficar só com os pontos que interessam
        clipPointCloud(cloud_with_hand_clipped, cloud_with_hand_clipped, "z", backward_limit, foward_limit); //É feio mas funciona

        //Vamos pegar o comprimento da point cloud com as caixas fazer crop assim o proximo plano de certeza que será a mesa
        getPointCloudSize(cloud_with_objects, &left_limit, &right_limit, &backward_limit, &foward_limit);

        clipPointCloud(cloud_with_hand_clipped, cloud_with_hand_clipped, "z", backward_limit, foward_limit, true); //Vai retornar a point cloud sem as caixas

        extractPlaneFromPointCloud(cloud_with_hand_clipped, cloud_with_hand_on_plane, coefficients, inliers, seg);

        discartPlaneFromPointCloud(cloud_with_hand_clipped, cloud_with_hand_off_plane, inliers);

        removeOutliers(cloud_with_hand_off_plane, cloud_with_hand_off_plane); //Havia uns pontos perdidos, isso foi para limpar melhor e no passo seguinte só retornar 2 clusters

        bool isConfirmationHandVisible;

        cloud_with_hand = getHands(cloud_with_hand_off_plane, &isConfirmationHandVisible);

        std::cout << "isConfirmationHandVisible: " << isConfirmationHandVisible << std::endl;

        if (isConfirmationHandVisible)
        {
            debugText << "Number of Hands = 2" << std::endl;
        }
        else
        {
            debugText << "Number of Hands = 1" << std::endl;
        }

        //0 = marcador || 1 = apagador
        int objectType = getObjectType(cloud_with_hand);
        //std::cout << "objectType int: " << objectType << std::endl;

        if (objectType == 0)
        {
            debugText << "Object Type = Marcador" << std::endl;
        }
        else
        {
            debugText << "Object Type = Apagador" << std::endl;
        }

        cloud_to_viewer = cloud_with_hand;

#pragma endregion PASSO_4

#pragma region PASSO_5

        pcl::PointXYZRGBA lowestPoint = getLowestPoint(cloud_with_hand);

        std::cout << "Posição do Objeto (x, y, z): "
                  << "(" << lowestPoint.x << ", " << lowestPoint.y << ", " << lowestPoint.z << ")" << std::endl;

        //std::cout << "y_min: " << y_min << std::endl;

        bool isTouching = false;

        if (lowestPoint.y < -0.15)
        {
            std::cout << "Objeto detectado: Afastado da mesa" << std::endl;
            debugText << "Objeto detectado: Afastado da mesa" << std::endl;
            isTouching = false;
        }
        else
        {
            std::cout << "Objeto detectado: Junto à mesa" << std::endl;
            debugText << "Objeto detectado: Junto à mesa" << std::endl;
            isTouching = true;
        }

        //Se não estiver tocando na mesa e o objeto detectado for um marcador veremos para qual direção o marcador aponta
        if (!isTouching && objectType == 0)
        {
            osg::Vec3 objectDirection = getDirection(cloud_with_hand);

            double x = objectDirection.x();
            double y = objectDirection.y();
            double z = objectDirection.z();

            std::cout << "Direção do Objeto (x, y, z): "
                      << "(" << x << ", " << y << ", " << z << ")" << std::endl;

            debugText << "Direção do Objeto (x, y, z): "
                      << "(" << x << ", " << y << ", " << z << ")" << std::endl;

            //Verifica a qual objeto estamos apontando
            bool collided = detectCollisions(lowestPoint, objectDirection, cloud_with_objects, selected_object);

#pragma endregion PASSO_5

#pragma region PASSO_6

            //Se houver uma point cloud nessa direção então iremos guarda-la
            if (selected_object->size() > 0)
            {
                selected_object = getLargerPointCloudCluster(selected_object);

                //Se for detectada uma segunda mão iremos calcular e guardar a cor média dos pontos da point cloud selecionada
                if (isConfirmationHandVisible)
                {
                    selected_object_color = getColorAverage(selected_object);

                    std::cout << " Cor média (RGB) objeto selecionado : "
                              << "(" << selected_object_color.r << ", " << selected_object_color.g << ", " << selected_object_color.b << ")" << std::endl;

                    debugText << " Cor média (RGB) objeto selecionado : "
                              << "(" << selected_object_color.r << ", " << selected_object_color.g << ", " << selected_object_color.b << ")" << std::endl;
                }
            }
            else
            {
                std::cout << "Não foi encontrado nenhum objeto nesta direção" << std::endl;
                debugText<< "Não foi encontrado nenhum objeto nesta direção" << std::endl;
            }

#pragma endregion PASSO_6

            /////////////////////////////////////////  debug direction
            cube_coeff.values.resize(10);

            cube_coeff.values[0] = x;

            cube_coeff.values[1] = y;

            cube_coeff.values[2] = z;

            cube_coeff.values[3] = cube_coeff.values[4] = cube_coeff.values[5] = cube_coeff.values[6] = 0;

            cube_coeff.values[7] = cube_coeff.values[8] = cube_coeff.values[9] = 0.1;
            ///////////////////////////////////////////////// debug direction
        }
    }

    // go through the point cloud and generate a RGB image and a range image
    int size = cloud_in->height * cloud_in->width * 3;

    unsigned char *data = (unsigned char *)calloc(size, sizeof(unsigned char));

    unsigned char *dataDepth = (unsigned char *)calloc(size, sizeof(unsigned char));

    createImageFromPointCloud(cloud_in, data, dataDepth);

    // create a texture from the RGB image and use depth data to fill the z-buffer

    osg::ref_ptr<osg::Geode> orthoTextureGeode = new osg::Geode;

    createOrthoTexture(orthoTextureGeode, data, dataDepth, cloud_in->width, cloud_in->height);

    // create an orthographic camera imaging the texture and a perspective camera based on the camera's pose and intrinsics

    osg::ref_ptr<osg::Camera> camera1 = new osg::Camera;

    osg::ref_ptr<osg::Camera> camera2 = new osg::Camera;

    osg::ref_ptr<osgText::Text> textFrames;
    textFrames = createText(osg::Vec3(10.0f, 640.0f, 0.0f), "", 20.0f);

    osg::ref_ptr<osg::Camera> camera3 = new osg::Camera;
    camera3 = createHUD(&textFrames);
    textFrames->setText(debugText.str());

    createVirtualCameras(camera1, camera2, orthoTextureGeode, camera_pitch, camera_roll, camera_height);

    // add the two cameras to the scene's root node

    osg::ref_ptr<osg::Group> root = new osg::Group;

    root->addChild(camera1.get());

    root->addChild(camera2.get());

    root->addChild(camera3.get());

// Passo 7 - Não conseguimos criar o objeto
#pragma region PASSO_7
/*
    //osg::Vec4f *selectedColor = new osg::Vec4f(selected_object_color.r, selected_object_color.g, selected_object_color.b, 1.0f);
    osg::Vec4f *selectedColor = new osg::Vec4f(255, 0, 0, 1.0f);
    osg::Vec3f *centerVector = new osg::Vec3f(0, 0, 0);

    osg::ref_ptr<osg::ShapeDrawable> cylShapeDrawable = new osg::ShapeDrawable(new osg::Cylinder(*centerVector, 0.1, 0.1));

    cylShapeDrawable->setColor(*selectedColor);

    brush->addDrawable(cylShapeDrawable);

    osg::ref_ptr<osg::PositionAttitudeTransform> brushTransform = new osg::PositionAttitudeTransform;

    brushTransform->setPosition(osg::Vec3(0, 0, 0));
    brushTransform->setScale(osg::Vec3(1, 1, 1));
    brushTransform->addChild(brush.get());

    osg::StateSet *ss = brush->getOrCreateStateSet();

    ss->setMode(GL_LIGHT0, osg::StateAttribute::OFF);
    ss->setMode(GL_LIGHT1, osg::StateAttribute::ON);

    camera2->addChild(brushTransform);

    */
#pragma endregion PASSO_7

    // create a root's viewer

    osgViewer::Viewer viewer;

    viewer.setUpViewInWindow(0, 10, 640, 480);

    viewer.setSceneData(root.get());

    //Setup do PCLviewer para fins de debug
    boost::shared_ptr<pcl::visualization::PCLVisualizer> viewerPCL;
    viewerPCL = rgbVis(cloud_to_viewer); //Envia para o PCL a nuvem de pontos

    if (selected_object->size() > 0)
    {
        pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZRGBA> rgb(selected_object);
        viewerPCL->addPointCloud<pcl::PointXYZRGBA>(selected_object, rgb, "sample cloud 2");

        ////////////////////////////////////////////////////////////// debug direction
        viewerPCL->removeShape("cube");

        viewerPCL->addCube(cube_coeff, "cube");
        ///////////////////////////////////////////////////////////// debug direction
    }

    // Gera imagens consecutivas até o momento em que a tecla "ESC" é pressionada
    while (!viewer.done())
    {

        viewerPCL->spinOnce(100); //Debug
        viewer.frame();
    }
}
