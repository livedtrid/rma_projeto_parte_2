# RMA_Projeto_Parte_2

Repositório para a 2ª parte do projeto de RMA

# Comandos para abrir as nuvens de pontos, talvez renomear os ficheiros seja uma boa ideia.
# Exemplo para abrir no pcl com eixo visível: pcl_viewer ../clouds_2018_draw/seq_1/background_no_objs_1/129950502913.pcd -ax 1

# Abrir na nossa maravilhosa aplicação

# Sequência 1

# Abrir background_no_objs + background_objs + draw
./rma_projeto_parte_2 ../clouds_2018_draw/seq_1/background_no_objs_1/129950502913.pcd ../clouds_2018_draw/seq_1/background_objs_1/129950567581.pcd ../clouds_2018_draw/seq_1/draw_objs_1/129950585005.pcd //branco
./rma_projeto_parte_2 ../clouds_2018_draw/seq_1/background_no_objs_1/129950502913.pcd ../clouds_2018_draw/seq_1/background_objs_1/129950567581.pcd ../clouds_2018_draw/seq_1/draw_objs_1/129950585102.pcd //azul
./rma_projeto_parte_2 ../clouds_2018_draw/seq_1/background_no_objs_1/129950502913.pcd ../clouds_2018_draw/seq_1/background_objs_1/129950567581.pcd ../clouds_2018_draw/seq_1/draw_objs_1/129950585203.pcd //entre o azul e o vermelho
./rma_projeto_parte_2 ../clouds_2018_draw/seq_1/background_no_objs_1/129950502913.pcd ../clouds_2018_draw/seq_1/background_objs_1/129950567581.pcd ../clouds_2018_draw/seq_1/draw_objs_1/129950585337.pcd //vermelho
./rma_projeto_parte_2 ../clouds_2018_draw/seq_1/background_no_objs_1/129950502913.pcd ../clouds_2018_draw/seq_1/background_objs_1/129950567581.pcd ../clouds_2018_draw/seq_1/draw_objs_1/129950585418.pcd //vermelho mais atras
./rma_projeto_parte_2 ../clouds_2018_draw/seq_1/background_no_objs_1/129950502913.pcd ../clouds_2018_draw/seq_1/background_objs_1/129950567581.pcd ../clouds_2018_draw/seq_1/draw_objs_1/129950585549.pcd //Azul
./rma_projeto_parte_2 ../clouds_2018_draw/seq_1/background_no_objs_1/129950502913.pcd ../clouds_2018_draw/seq_1/background_objs_1/129950567581.pcd ../clouds_2018_draw/seq_1/draw_objs_1/129950585559.pcd //Azul
./rma_projeto_parte_2 ../clouds_2018_draw/seq_1/background_no_objs_1/129950502913.pcd ../clouds_2018_draw/seq_1/background_objs_1/129950567581.pcd ../clouds_2018_draw/seq_1/draw_objs_1/129950585646.pcd //Branco
./rma_projeto_parte_2 ../clouds_2018_draw/seq_1/background_no_objs_1/129950502913.pcd ../clouds_2018_draw/seq_1/background_objs_1/129950567581.pcd ../clouds_2018_draw/seq_1/draw_objs_1/129950585934.pcd //Azul Não toca na mesa
./rma_projeto_parte_2 ../clouds_2018_draw/seq_1/background_no_objs_1/129950502913.pcd ../clouds_2018_draw/seq_1/background_objs_1/129950567581.pcd ../clouds_2018_draw/seq_1/draw_objs_1/129950586013.pcd //Azul Não toca na mesa

# Abrir background_no_objs + background_objs + erase
./rma_projeto_parte_2 ../clouds_2018_draw/seq_1/background_no_objs_1/129950502913.pcd ../clouds_2018_draw/seq_1/background_objs_1/129950567581.pcd ../clouds_2018_draw/seq_1/erase_objs_1/129950592127.pcd //afastado da mesa
./rma_projeto_parte_2 ../clouds_2018_draw/seq_1/background_no_objs_1/129950502913.pcd ../clouds_2018_draw/seq_1/background_objs_1/129950567581.pcd ../clouds_2018_draw/seq_1/erase_objs_1/129950592283.pcd 
./rma_projeto_parte_2 ../clouds_2018_draw/seq_1/background_no_objs_1/129950502913.pcd ../clouds_2018_draw/seq_1/background_objs_1/129950567581.pcd ../clouds_2018_draw/seq_1/erase_objs_1/129950592579.pcd 
./rma_projeto_parte_2 ../clouds_2018_draw/seq_1/background_no_objs_1/129950502913.pcd ../clouds_2018_draw/seq_1/background_objs_1/129950567581.pcd ../clouds_2018_draw/seq_1/erase_objs_1/129950592672.pcd 
./rma_projeto_parte_2 ../clouds_2018_draw/seq_1/background_no_objs_1/129950502913.pcd ../clouds_2018_draw/seq_1/background_objs_1/129950567581.pcd ../clouds_2018_draw/seq_1/erase_objs_1/129950592989.pcd 
./rma_projeto_parte_2 ../clouds_2018_draw/seq_1/background_no_objs_1/129950502913.pcd ../clouds_2018_draw/seq_1/background_objs_1/129950567581.pcd ../clouds_2018_draw/seq_1/erase_objs_1/129950593080.pcd 
./rma_projeto_parte_2 ../clouds_2018_draw/seq_1/background_no_objs_1/129950502913.pcd ../clouds_2018_draw/seq_1/background_objs_1/129950567581.pcd ../clouds_2018_draw/seq_1/erase_objs_1/129950593171.pcd

# Abrir background_no_objs + background_objs + point
./rma_projeto_parte_2 ../clouds_2018_draw/seq_1/background_no_objs_1/129950502913.pcd ../clouds_2018_draw/seq_1/background_objs_1/129950567581.pcd ../clouds_2018_draw/seq_1/point_objs_1/129950573675.pcd //afastado da mesa
./rma_projeto_parte_2 ../clouds_2018_draw/seq_1/background_no_objs_1/129950502913.pcd ../clouds_2018_draw/seq_1/background_objs_1/129950567581.pcd ../clouds_2018_draw/seq_1/point_objs_1/129950574048.pcd 
./rma_projeto_parte_2 ../clouds_2018_draw/seq_1/background_no_objs_1/129950502913.pcd ../clouds_2018_draw/seq_1/background_objs_1/129950567581.pcd ../clouds_2018_draw/seq_1/point_objs_1/129950574265.pcd 
./rma_projeto_parte_2 ../clouds_2018_draw/seq_1/background_no_objs_1/129950502913.pcd ../clouds_2018_draw/seq_1/background_objs_1/129950567581.pcd ../clouds_2018_draw/seq_1/point_objs_1/129950574431.pcd //Seleciona o vermelho
./rma_projeto_parte_2 ../clouds_2018_draw/seq_1/background_no_objs_1/129950502913.pcd ../clouds_2018_draw/seq_1/background_objs_1/129950567581.pcd ../clouds_2018_draw/seq_1/point_objs_1/129950574621.pcd 
./rma_projeto_parte_2 ../clouds_2018_draw/seq_1/background_no_objs_1/129950502913.pcd ../clouds_2018_draw/seq_1/background_objs_1/129950567581.pcd ../clouds_2018_draw/seq_1/point_objs_1/129950575590.pcd //Seleciona o azul
./rma_projeto_parte_2 ../clouds_2018_draw/seq_1/background_no_objs_1/129950502913.pcd ../clouds_2018_draw/seq_1/background_objs_1/129950567581.pcd ../clouds_2018_draw/seq_1/point_objs_1/129950575780.pcd


# Sequência 2

# Abrir background_no_objs + background_objs + draw
./rma_projeto_parte_2 ../clouds_2018_draw/seq_2/background_no_objs_2/129950502913.pcd ../clouds_2018_draw/seq_2/background_objs_2/129950517743.pcd ../clouds_2018_draw/seq_2/draw_objs_2/129950541056.pcd
./rma_projeto_parte_2 ../clouds_2018_draw/seq_2/background_no_objs_2/129950502913.pcd ../clouds_2018_draw/seq_2/background_objs_2/129950517743.pcd ../clouds_2018_draw/seq_2/draw_objs_2/129950541299.pcd
./rma_projeto_parte_2 ../clouds_2018_draw/seq_2/background_no_objs_2/129950502913.pcd ../clouds_2018_draw/seq_2/background_objs_2/129950517743.pcd ../clouds_2018_draw/seq_2/draw_objs_2/129950541447.pcd
./rma_projeto_parte_2 ../clouds_2018_draw/seq_2/background_no_objs_2/129950502913.pcd ../clouds_2018_draw/seq_2/background_objs_2/129950517743.pcd ../clouds_2018_draw/seq_2/draw_objs_2/129950541523.pcd
./rma_projeto_parte_2 ../clouds_2018_draw/seq_2/background_no_objs_2/129950502913.pcd ../clouds_2018_draw/seq_2/background_objs_2/129950517743.pcd ../clouds_2018_draw/seq_2/draw_objs_2/129950541706.pcd
./rma_projeto_parte_2 ../clouds_2018_draw/seq_2/background_no_objs_2/129950502913.pcd ../clouds_2018_draw/seq_2/background_objs_2/129950517743.pcd ../clouds_2018_draw/seq_2/draw_objs_2/129950542065.pcd
./rma_projeto_parte_2 ../clouds_2018_draw/seq_2/background_no_objs_2/129950502913.pcd ../clouds_2018_draw/seq_2/background_objs_2/129950517743.pcd ../clouds_2018_draw/seq_2/draw_objs_2/129950542152.pcd
./rma_projeto_parte_2 ../clouds_2018_draw/seq_2/background_no_objs_2/129950502913.pcd ../clouds_2018_draw/seq_2/background_objs_2/129950517743.pcd ../clouds_2018_draw/seq_2/draw_objs_2/129950542245.pcd

# Abrir background_no_objs + background_objs + erase
./rma_projeto_parte_2 ../clouds_2018_draw/seq_2/background_no_objs_2/129950502913.pcd ../clouds_2018_draw/seq_2/background_objs_2/129950517743.pcd ../clouds_2018_draw/seq_2/erase_objs_2/129950546645.pcd
./rma_projeto_parte_2 ../clouds_2018_draw/seq_2/background_no_objs_2/129950502913.pcd ../clouds_2018_draw/seq_2/background_objs_2/129950517743.pcd ../clouds_2018_draw/seq_2/erase_objs_2/129950546822.pcd
./rma_projeto_parte_2 ../clouds_2018_draw/seq_2/background_no_objs_2/129950502913.pcd ../clouds_2018_draw/seq_2/background_objs_2/129950517743.pcd ../clouds_2018_draw/seq_2/erase_objs_2/129950547286.pcd
./rma_projeto_parte_2 ../clouds_2018_draw/seq_2/background_no_objs_2/129950502913.pcd ../clouds_2018_draw/seq_2/background_objs_2/129950517743.pcd ../clouds_2018_draw/seq_2/erase_objs_2/129950547687.pcd
./rma_projeto_parte_2 ../clouds_2018_draw/seq_2/background_no_objs_2/129950502913.pcd ../clouds_2018_draw/seq_2/background_objs_2/129950517743.pcd ../clouds_2018_draw/seq_2/erase_objs_2/129950547824.pcd
./rma_projeto_parte_2 ../clouds_2018_draw/seq_2/background_no_objs_2/129950502913.pcd ../clouds_2018_draw/seq_2/background_objs_2/129950517743.pcd ../clouds_2018_draw/seq_2/erase_objs_2/129950548208.pcd

# Abrir background_no_objs + background_objs + point
./rma_projeto_parte_2 ../clouds_2018_draw/seq_2/background_no_objs_2/129950502913.pcd ../clouds_2018_draw/seq_2/background_objs_2/129950517743.pcd ../clouds_2018_draw/seq_2/point_objs_2/129950529167.pcd //boom
./rma_projeto_parte_2 ../clouds_2018_draw/seq_2/background_no_objs_2/129950502913.pcd ../clouds_2018_draw/seq_2/background_objs_2/129950517743.pcd ../clouds_2018_draw/seq_2/point_objs_2/129950529227.pcd
./rma_projeto_parte_2 ../clouds_2018_draw/seq_2/background_no_objs_2/129950502913.pcd ../clouds_2018_draw/seq_2/background_objs_2/129950517743.pcd ../clouds_2018_draw/seq_2/point_objs_2/129950529487.pcd
./rma_projeto_parte_2 ../clouds_2018_draw/seq_2/background_no_objs_2/129950502913.pcd ../clouds_2018_draw/seq_2/background_objs_2/129950517743.pcd ../clouds_2018_draw/seq_2/point_objs_2/129950529833.pcd
./rma_projeto_parte_2 ../clouds_2018_draw/seq_2/background_no_objs_2/129950502913.pcd ../clouds_2018_draw/seq_2/background_objs_2/129950517743.pcd ../clouds_2018_draw/seq_2/point_objs_2/129950532780.pcd
./rma_projeto_parte_2 ../clouds_2018_draw/seq_2/background_no_objs_2/129950502913.pcd ../clouds_2018_draw/seq_2/background_objs_2/129950517743.pcd ../clouds_2018_draw/seq_2/point_objs_2/129950532853.pcd

# Sequência 3

# Abrir background_no_objs + background_objs + draw
./rma_projeto_parte_2 ../clouds_2018_draw/seq_3/background_no_objs_3/129976532142.pcd ../clouds_2018_draw/seq_3/background_objs_3/129976479791.pcd ../clouds_2018_draw/seq_3/draw_objs_3/129976478626.pcd
./rma_projeto_parte_2 ../clouds_2018_draw/seq_3/background_no_objs_3/129976532142.pcd ../clouds_2018_draw/seq_3/background_objs_3/129976479791.pcd ../clouds_2018_draw/seq_3/draw_objs_3/129976478767.pcd
./rma_projeto_parte_2 ../clouds_2018_draw/seq_3/background_no_objs_3/129976532142.pcd ../clouds_2018_draw/seq_3/background_objs_3/129976479791.pcd ../clouds_2018_draw/seq_3/draw_objs_3/129976478894.pcd
./rma_projeto_parte_2 ../clouds_2018_draw/seq_3/background_no_objs_3/129976532142.pcd ../clouds_2018_draw/seq_3/background_objs_3/129976479791.pcd ../clouds_2018_draw/seq_3/draw_objs_3/129976479021.pcd
./rma_projeto_parte_2 ../clouds_2018_draw/seq_3/background_no_objs_3/129976532142.pcd ../clouds_2018_draw/seq_3/background_objs_3/129976479791.pcd ../clouds_2018_draw/seq_3/draw_objs_3/129976479086.pcd
./rma_projeto_parte_2 ../clouds_2018_draw/seq_3/background_no_objs_3/129976532142.pcd ../clouds_2018_draw/seq_3/background_objs_3/129976479791.pcd ../clouds_2018_draw/seq_3/draw_objs_3/129976479187.pcd
./rma_projeto_parte_2 ../clouds_2018_draw/seq_3/background_no_objs_3/129976532142.pcd ../clouds_2018_draw/seq_3/background_objs_3/129976479791.pcd ../clouds_2018_draw/seq_3/draw_objs_3/129976479277.pcd

# Abrir background_no_objs + background_objs + erase
./rma_projeto_parte_2 ../clouds_2018_draw/seq_3/background_no_objs_3/129976532142.pcd ../clouds_2018_draw/seq_3/background_objs_3/129976479791.pcd ../clouds_2018_draw/seq_3/erase_objs_3/129976494840.pcd
./rma_projeto_parte_2 ../clouds_2018_draw/seq_3/background_no_objs_3/129976532142.pcd ../clouds_2018_draw/seq_3/background_objs_3/129976479791.pcd ../clouds_2018_draw/seq_3/erase_objs_3/129976495013.pcd
./rma_projeto_parte_2 ../clouds_2018_draw/seq_3/background_no_objs_3/129976532142.pcd ../clouds_2018_draw/seq_3/background_objs_3/129976479791.pcd ../clouds_2018_draw/seq_3/erase_objs_3/129976495303.pcd
./rma_projeto_parte_2 ../clouds_2018_draw/seq_3/background_no_objs_3/129976532142.pcd ../clouds_2018_draw/seq_3/background_objs_3/129976479791.pcd ../clouds_2018_draw/seq_3/erase_objs_3/129976495443.pcd
./rma_projeto_parte_2 ../clouds_2018_draw/seq_3/background_no_objs_3/129976532142.pcd ../clouds_2018_draw/seq_3/background_objs_3/129976479791.pcd ../clouds_2018_draw/seq_3/erase_objs_3/129976495545.pcd
./rma_projeto_parte_2 ../clouds_2018_draw/seq_3/background_no_objs_3/129976532142.pcd ../clouds_2018_draw/seq_3/background_objs_3/129976479791.pcd ../clouds_2018_draw/seq_3/erase_objs_3/129976495685.pcd
./rma_projeto_parte_2 ../clouds_2018_draw/seq_3/background_no_objs_3/129976532142.pcd ../clouds_2018_draw/seq_3/background_objs_3/129976479791.pcd ../clouds_2018_draw/seq_3/erase_objs_3/129976495776.pcd
./rma_projeto_parte_2 ../clouds_2018_draw/seq_3/background_no_objs_3/129976532142.pcd ../clouds_2018_draw/seq_3/background_objs_3/129976479791.pcd ../clouds_2018_draw/seq_3/erase_objs_3/129976495895.pcd

# Abrir background_no_objs + background_objs + point
./rma_projeto_parte_2 ../clouds_2018_draw/seq_3/background_no_objs_3/129976532142.pcd ../clouds_2018_draw/seq_3/background_objs_3/129976479791.pcd ../clouds_2018_draw/seq_3/point_objs_3/129976476331.pcd
./rma_projeto_parte_2 ../clouds_2018_draw/seq_3/background_no_objs_3/129976532142.pcd ../clouds_2018_draw/seq_3/background_objs_3/129976479791.pcd ../clouds_2018_draw/seq_3/point_objs_3/129976476842.pcd
./rma_projeto_parte_2 ../clouds_2018_draw/seq_3/background_no_objs_3/129976532142.pcd ../clouds_2018_draw/seq_3/background_objs_3/129976479791.pcd ../clouds_2018_draw/seq_3/point_objs_3/129976477342.pcd
./rma_projeto_parte_2 ../clouds_2018_draw/seq_3/background_no_objs_3/129976532142.pcd ../clouds_2018_draw/seq_3/background_objs_3/129976479791.pcd ../clouds_2018_draw/seq_3/point_objs_3/129976477563.pcd
./rma_projeto_parte_2 ../clouds_2018_draw/seq_3/background_no_objs_3/129976532142.pcd ../clouds_2018_draw/seq_3/background_objs_3/129976479791.pcd ../clouds_2018_draw/seq_3/point_objs_3/129976477629.pcd
./rma_projeto_parte_2 ../clouds_2018_draw/seq_3/background_no_objs_3/129976532142.pcd ../clouds_2018_draw/seq_3/background_objs_3/129976479791.pcd ../clouds_2018_draw/seq_3/point_objs_3/129976477932.pcd
./rma_projeto_parte_2 ../clouds_2018_draw/seq_3/background_no_objs_3/129976532142.pcd ../clouds_2018_draw/seq_3/background_objs_3/129976479791.pcd ../clouds_2018_draw/seq_3/point_objs_3/129976478015.pcd
./rma_projeto_parte_2 ../clouds_2018_draw/seq_3/background_no_objs_3/129976532142.pcd ../clouds_2018_draw/seq_3/background_objs_3/129976479791.pcd ../clouds_2018_draw/seq_3/point_objs_3/129976478306.pcd
./rma_projeto_parte_2 ../clouds_2018_draw/seq_3/background_no_objs_3/129976532142.pcd ../clouds_2018_draw/seq_3/background_objs_3/129976479791.pcd ../clouds_2018_draw/seq_3/point_objs_3/129976478340.pcd

#Abrir no PCL:

# Sequência 1
pcl_viewer ../clouds_2018_draw/seq_1/background_no_objs_1/129950502913.pcd -ax 1
pcl_viewer ../clouds_2018_draw/seq_1/background_objs_1/129950567581.pcd -ax 1

pcl_viewer ../clouds_2018_draw/seq_1/draw_objs_1/129950585005.pcd -ax 1 //aponta para o branco
pcl_viewer ../clouds_2018_draw/seq_1/draw_objs_1/129950585102.pcd -ax 1 //aponta para o azul
pcl_viewer ../clouds_2018_draw/seq_1/draw_objs_1/129950585203.pcd -ax 1 //aponta para entre o azul e o vermelho
pcl_viewer ../clouds_2018_draw/seq_1/draw_objs_1/129950585337.pcd -ax 1 //aponta para o vermelho
pcl_viewer ../clouds_2018_draw/seq_1/draw_objs_1/129950585418.pcd -ax 1 //aponta para o vermelho
pcl_viewer ../clouds_2018_draw/seq_1/draw_objs_1/129950585549.pcd -ax 1 //aponta para o azul
pcl_viewer ../clouds_2018_draw/seq_1/draw_objs_1/129950585559.pcd -ax 1 //aponta para o azul
pcl_viewer ../clouds_2018_draw/seq_1/draw_objs_1/129950585646.pcd -ax 1 //aponta para o branco
pcl_viewer ../clouds_2018_draw/seq_1/draw_objs_1/129950585934.pcd -ax 1 //aponta para o azul, elevado
pcl_viewer ../clouds_2018_draw/seq_1/draw_objs_1/129950586013.pcd -ax 1 //aponta para o azul, elevado

pcl_viewer ../clouds_2018_draw/seq_1/erase_objs_1/129950592127.pcd -ax 1 //afastado da mesa
pcl_viewer ../clouds_2018_draw/seq_1/erase_objs_1/129950592283.pcd -ax 1 //apontado para o azul
pcl_viewer ../clouds_2018_draw/seq_1/erase_objs_1/129950592579.pcd -ax 1 //apontado para o branco, toca na mesa
pcl_viewer ../clouds_2018_draw/seq_1/erase_objs_1/129950592672.pcd -ax 1 //apontado para o azul, toca na mesa
pcl_viewer ../clouds_2018_draw/seq_1/erase_objs_1/129950592989.pcd -ax 1 //apontado para o vermelho, toca na mesa
pcl_viewer ../clouds_2018_draw/seq_1/erase_objs_1/129950593080.pcd -ax 1 //apontado para o azul, toca na mesa
pcl_viewer ../clouds_2018_draw/seq_1/erase_objs_1/129950593171.pcd -ax 1 //apontado para o branco, toca na mesa

pcl_viewer ../clouds_2018_draw/seq_1/point_objs_1/129950573675.pcd -ax 1 //afastado da mesa
pcl_viewer ../clouds_2018_draw/seq_1/point_objs_1/129950574048.pcd -ax 1 //apontado para o branco
pcl_viewer ../clouds_2018_draw/seq_1/point_objs_1/129950574265.pcd -ax 1 //apontado para o azul
pcl_viewer ../clouds_2018_draw/seq_1/point_objs_1/129950574431.pcd -ax 1 //apontado para o vermelho, mão de confirmação
pcl_viewer ../clouds_2018_draw/seq_1/point_objs_1/129950574621.pcd -ax 1 //apontado para o vermelho
pcl_viewer ../clouds_2018_draw/seq_1/point_objs_1/129950575590.pcd -ax 1 //apontado para o azul, mão de confirmação
pcl_viewer ../clouds_2018_draw/seq_1/point_objs_1/129950575780.pcd -ax 1 //apontado para o azul

# Sequência 2
pcl_viewer ../clouds_2018_draw/seq_2/background_no_objs_2/129950502913.pcd -ax 1
pcl_viewer ../clouds_2018_draw/seq_2/background_objs_2/129950517743.pcd -ax 1

pcl_viewer ../clouds_2018_draw/seq_2/draw_objs_2/129950541056.pcd -ax 1
pcl_viewer ../clouds_2018_draw/seq_2/draw_objs_2/129950541299.pcd -ax 1
pcl_viewer ../clouds_2018_draw/seq_2/draw_objs_2/129950541447.pcd -ax 1
pcl_viewer ../clouds_2018_draw/seq_2/draw_objs_2/129950541523.pcd -ax 1
pcl_viewer ../clouds_2018_draw/seq_2/draw_objs_2/129950541706.pcd -ax 1
pcl_viewer ../clouds_2018_draw/seq_2/draw_objs_2/129950542065.pcd -ax 1
pcl_viewer ../clouds_2018_draw/seq_2/draw_objs_2/129950542152.pcd -ax 1
pcl_viewer ../clouds_2018_draw/seq_2/draw_objs_2/129950542245.pcd -ax 1

pcl_viewer ../clouds_2018_draw/seq_2/erase_objs_2/129950546645.pcd -ax 1
pcl_viewer ../clouds_2018_draw/seq_2/erase_objs_2/129950546822.pcd -ax 1
pcl_viewer ../clouds_2018_draw/seq_2/erase_objs_2/129950547286.pcd -ax 1
pcl_viewer ../clouds_2018_draw/seq_2/erase_objs_2/129950547687.pcd -ax 1
pcl_viewer ../clouds_2018_draw/seq_2/erase_objs_2/129950547824.pcd -ax 1
pcl_viewer ../clouds_2018_draw/seq_2/erase_objs_2/129950548208.pcd -ax 1

pcl_viewer ../clouds_2018_draw/seq_2/point_objs_2/129950529167.pcd -ax 1
pcl_viewer ../clouds_2018_draw/seq_2/point_objs_2/129950529227.pcd -ax 1
pcl_viewer ../clouds_2018_draw/seq_2/point_objs_2/129950529487.pcd -ax 1
pcl_viewer ../clouds_2018_draw/seq_2/point_objs_2/129950529833.pcd -ax 1
pcl_viewer ../clouds_2018_draw/seq_2/point_objs_2/129950532780.pcd -ax 1
pcl_viewer ../clouds_2018_draw/seq_2/point_objs_2/129950532853.pcd -ax 1

# Sequência 3
pcl_viewer ../clouds_2018_draw/seq_3/background_no_objs_3/129976532142.pcd -ax 1
pcl_viewer ../clouds_2018_draw/seq_3/background_objs_3/129976479791.pcd -ax 1

pcl_viewer ../clouds_2018_draw/seq_3/draw_objs_3/129976478626.pcd -ax 1
pcl_viewer ../clouds_2018_draw/seq_3/draw_objs_3/129976478767.pcd -ax 1
pcl_viewer ../clouds_2018_draw/seq_3/draw_objs_3/129976478894.pcd -ax 1
pcl_viewer ../clouds_2018_draw/seq_3/draw_objs_3/129976479021.pcd -ax 1
pcl_viewer ../clouds_2018_draw/seq_3/draw_objs_3/129976479086.pcd -ax 1
pcl_viewer ../clouds_2018_draw/seq_3/draw_objs_3/129976479187.pcd -ax 1
pcl_viewer ../clouds_2018_draw/seq_3/draw_objs_3/129976479277.pcd -ax 1

pcl_viewer ../clouds_2018_draw/seq_3/erase_objs_3/129976494840.pcd -ax 1
pcl_viewer ../clouds_2018_draw/seq_3/erase_objs_3/129976495013.pcd -ax 1
pcl_viewer ../clouds_2018_draw/seq_3/erase_objs_3/129976495303.pcd -ax 1
pcl_viewer ../clouds_2018_draw/seq_3/erase_objs_3/129976495443.pcd -ax 1
pcl_viewer ../clouds_2018_draw/seq_3/erase_objs_3/129976495545.pcd -ax 1
pcl_viewer ../clouds_2018_draw/seq_3/erase_objs_3/129976495685.pcd -ax 1
pcl_viewer ../clouds_2018_draw/seq_3/erase_objs_3/129976495776.pcd -ax 1
pcl_viewer ../clouds_2018_draw/seq_3/erase_objs_3/129976495895.pcd -ax 1

pcl_viewer ../clouds_2018_draw/seq_3/point_objs_3/129976476331.pcd -ax 1
pcl_viewer ../clouds_2018_draw/seq_3/point_objs_3/129976476842.pcd -ax 1
pcl_viewer ../clouds_2018_draw/seq_3/point_objs_3/129976477342.pcd -ax 1
pcl_viewer ../clouds_2018_draw/seq_3/point_objs_3/129976477563.pcd -ax 1
pcl_viewer ../clouds_2018_draw/seq_3/point_objs_3/129976477629.pcd -ax 1
pcl_viewer ../clouds_2018_draw/seq_3/point_objs_3/129976477932.pcd -ax 1
pcl_viewer ../clouds_2018_draw/seq_3/point_objs_3/129976478015.pcd -ax 1
pcl_viewer ../clouds_2018_draw/seq_3/point_objs_3/129976478306.pcd -ax 1
pcl_viewer ../clouds_2018_draw/seq_3/point_objs_3/129976478340.pcd -ax 1